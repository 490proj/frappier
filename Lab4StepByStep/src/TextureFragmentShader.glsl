#version 330 core

uniform sampler2D sTexUnit;

smooth in vec2 vVarTexCoords0;

void main()
{
    gl_FragColor = texture(sTexUnit, vVarTexCoords0.st);
}
