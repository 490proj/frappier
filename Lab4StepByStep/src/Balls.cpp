
#include "Balls.h"
#include <assert.h>

BallCollection::BallCollection()
{
	for (int i = 0; i < 15; i++)
	{
		m_Balls[i].ballNumber = i;
	}
	Reset();
}

void BallCollection::Reset()
{
	for (int i = 0; i < 15; i++)
	{
		m_Balls[i].velocity = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		m_Balls[i].rotation = vec4(0.0f, 0.0f, 0.0f, 0.0f);
		m_Balls[i].onTable = true;
	}
	m_Balls[0].position = vec4(10.0f - 2.36f, 2.75f + 0.1875f, 10.0f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[1].position = vec4(10.0f + 2.36f, 2.75f + 0.1875f, 10.0f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[2].position = vec4(10.0f + 2.36f + 0.1624f * 4, 2.75f + 0.1875f, 10.0f + 0.1875f * 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[3].position = vec4(10.0f + 2.36f + 0.1624f * 4, 2.75f + 0.1875f, 10.0f - 0.1875f * 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[4].position = vec4(10.0f + 2.36f + 0.1624f * 2, 2.75f + 0.1875f, 10.0f + 0.1875f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[5].position = vec4(10.0f + 2.36f + 0.1624f * 2, 2.75f + 0.1875f, 10.0f - 0.1875f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[6].position = vec4(10.0f + 2.36f + 0.1624f * 3, 2.75f + 0.1875f, 10.0f + 0.1875f / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[7].position = vec4(10.0f + 2.36f + 0.1624f * 4, 2.75f + 0.1875f, 10.0f - 0.1875f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[8].position = vec4(10.0f + 2.36f + 0.1624f * 2, 2.75f + 0.1875f, 10.0f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[9].position = vec4(10.0f + 2.36f + 0.1624f * 3, 2.75f + 0.1875f, 10.0f - 0.1875f / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[10].position = vec4(10.0f + 2.36f + 0.1624f, 2.75f + 0.1875f, 10.0f - 0.1875f / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[11].position = vec4(10.0f + 2.36f + 0.1624f, 2.75f + 0.1875f, 10.0f + 0.1875f / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[12].position = vec4(10.0f + 2.36f + 0.1624f * 4, 2.75f + 0.1875f, 10.0f, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[13].position = vec4(10.0f + 2.36f + 0.1624f * 3, 2.75f + 0.1875f, 10.0f + 0.1875f * 3 / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[14].position = vec4(10.0f + 2.36f + 0.1624f * 3, 2.75f + 0.1875f, 10.0f - 0.1875f * 3 / 2, 0.0f); // 0.0 or 1.0 at the end??
	m_Balls[15].position = vec4(10.0f + 2.36f + 0.1624f * 4, 2.75f + 0.1875f, 10.0f + 0.1875f, 0.0f);
}

Ball* BallCollection::operator[](int index) 
{
	assert(index>=0 && "The ball number you asked for is smaller than zero!");
	assert(index<=15 && "You cannot ask for a ball number greater than 15!");
	return &m_Balls[index];
}

vec4 BallCollection::getBallPosition(int number)
{
	return m_Balls[number].position;
}