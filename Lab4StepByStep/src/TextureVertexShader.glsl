#version 330

in vec4 vVertex;
in vec2 vTexCoord0;
uniform mat4 viewMat; 
smooth out vec2 vVarTexCoords0;

void main(void) {
	vVarTexCoords0 = vTexCoord0;
	gl_Position = viewMat*vVertex;
}
