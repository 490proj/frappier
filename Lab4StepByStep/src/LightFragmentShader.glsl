#version 330 core

//All positions are in world coordinates
uniform sampler2D texID;		//Texture unit number
uniform vec4 vAmbientColor;		//Ambient light
uniform vec4 vLightColor1;		//Light 1 color
uniform vec4 vLightPosition1;	//Light 1 position
uniform vec4 vLightHeading1;	//Light 1 heading (for spotlight)
uniform vec4 vLightColor2;		//Light 2 color
uniform vec4 vLightPosition2;	//Light 2 position
uniform vec4 vLightHeading2;	//Light 2 heading (for spotlight)
uniform vec4 vLightColor3;		//Light 3 color
uniform vec4 vLightPosition3;	//Light 3 position
uniform vec4 vLightHeading3;	//Light 3 heading (for spotlight)

uniform vec4 vCameraPosition;	//Camera position (for specular light)

smooth in vec4 vWorldCoords;	//Fragment world coordinates
smooth in vec2 vVarTexCoords0;	//Fragment texture coordinates
flat in vec3 vFlatNormals;		//Fragment surface normals (flat)

out vec4 gl_FragColor;			//Fragment output color

void main()
{
	//Compute the ambient color
	vec4 ambient = texture(texID, vVarTexCoords0.st)*vAmbientColor;
	
	//Compute the diffuse color from light 1 (no distance effect, no spotlight effect)
	vec3 vToLight1 = normalize(vLightPosition1.xyz-vWorldCoords.xyz);
	vec4 diffuse1 = texture(texID, vVarTexCoords0.st)*vLightColor1*max(0,dot(vFlatNormals,vToLight1));
	diffuse1 = diffuse1*max(0,pow(dot(vLightHeading1.xyz,-vToLight1),20));

	//Compute the diffuse color from light 2 (no distance effect, no spotlight effect)
	vec3 vToLight2 = normalize(vLightPosition2.xyz-vWorldCoords.xyz);
	vec4 diffuse2 = texture(texID, vVarTexCoords0.st)*vLightColor2*max(0,dot(vFlatNormals,vToLight2));
	diffuse2 = diffuse2*max(0,pow(dot(vLightHeading2.xyz,-vToLight2),20));

	//Compute the diffuse color from light 3 (no distance effect, no spotlight effect)
	vec3 vToLight3 = normalize(vLightPosition3.xyz-vWorldCoords.xyz);
	vec4 diffuse3 = texture(texID, vVarTexCoords0.st)*vLightColor3*max(0,dot(vFlatNormals,vToLight3));
	diffuse3 = diffuse3*max(0,pow(dot(vLightHeading3.xyz,-vToLight3),20));

	//Add light 2 specularity, assuming material is uncolored for specular light (no distance effect, no spotlight effect)
	//vec3 vReflection = reflect(-vToLight2,vFlatNormals);
	//vec3 vToCamera = normalize(vCameraPosition.xyz-vWorldCoords.xyz);
	//vec4 specular2 = vLightColor2*max(0,pow(dot(vReflection,vToCamera),120));

	//Generate final fragment color
    gl_FragColor = ambient + diffuse1 + diffuse2 + diffuse3;// + specular2;
}
